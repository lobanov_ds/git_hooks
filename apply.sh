#!/usr/bin/env bash

pip3 install flake8-per-file-ignores

for filename in commit-msg pre-commit.d/*
do
    sudo chmod +x ./$filename
done

git config --global core.hooksPath $(pwd)


